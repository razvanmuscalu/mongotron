package org.muscalu.mongotron.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Programme {

    private String title;
    private String type;

    public Programme(@JsonProperty("title") String title,
                     @JsonProperty("type") String type) {
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Programme)) return false;

        Programme that = (Programme) o;

        return new EqualsBuilder()
                .append(title, that.title)
                .isEquals();
    }

    public String getType() {
        return type;
    }
}
