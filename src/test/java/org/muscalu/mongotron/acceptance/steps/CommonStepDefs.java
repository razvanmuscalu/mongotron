package org.muscalu.mongotron.acceptance.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.muscalu.mongotron.acceptance.ResponseHolder;
import org.muscalu.mongotron.acceptance.SpringTest;
import org.muscalu.mongotron.web.Programme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SpringTest
public class CommonStepDefs {

    private static final String MONGOTRON_HOST = "http://localhost:4001/mongotron/";
    private static final String CONTENT_PATH = MONGOTRON_HOST + "content";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ResponseHolder responseHolder;

    @When("^I call the Content API for (.*) title$")
    public void callContentApiForTitle(String title) {
        ResponseEntity<String> response = restTemplate.getForEntity(CONTENT_PATH + "/" + title, String.class);
        responseHolder.set(response);
    }

    @When("^I call the Content API with (.*) query$")
    public void callContentApiForQuery(String query) {
        ResponseEntity<String> response = restTemplate.getForEntity(CONTENT_PATH + "?" + query, String.class);
        responseHolder.set(response);
    }

    @Then("^The following programmes should be returned$")
    public void theFollowingProgrammesAreReturned(DataTable dataTable) throws Exception {
        List<Programme> expectedProgrammes = dataTable.asList(Programme.class);

        if (expectedProgrammes.size() == 1) {
            Programme actualProgramme = OBJECT_MAPPER.readValue(responseHolder.getBody(), Programme.class);

            assertThat("should have expected title", actualProgramme.getTitle(), is(expectedProgrammes.get(0).getTitle()));
            assertThat("should have expected type", actualProgramme.getType(), is(expectedProgrammes.get(0).getType()));
            assertThat("should be expected programme", actualProgramme, is(expectedProgrammes.get(0)));
        } else {
            TypeReference<List<Programme>> typeRef = new TypeReference<List<Programme>>() {};
            List<Programme> actualProgrammes = OBJECT_MAPPER.readValue(responseHolder.getBody(), typeRef);

            assertThat("should return correct number of programmes", actualProgrammes, hasSize(expectedProgrammes.size()));
            expectedProgrammes.forEach(ep -> assertThat("should have expected programme", actualProgrammes, hasItem(ep)));
        }
    }

}
