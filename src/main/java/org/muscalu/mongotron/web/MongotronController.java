package org.muscalu.mongotron.web;

import org.muscalu.mongotron.service.ContentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.lang.System.currentTimeMillis;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/mongotron")
public class MongotronController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongotronController.class);

    private final ContentService contentService;

    @Autowired
    public MongotronController(ContentService contentService) {
        this.contentService = contentService;
    }

    @RequestMapping(method = GET, value = "/content/{title}", produces = APPLICATION_JSON_UTF8_VALUE)
    public Programme find(@PathVariable("title") String title) {
        long now = currentTimeMillis();

        Programme programme = contentService.getProgrammeByTitle(title);

        LOGGER.info("finding title={} took {} ms", title, currentTimeMillis() - now);

        return programme;
    }

    @RequestMapping(method = GET, value = "/content", produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Programme> query(@RequestParam(value = "type", required = false) String type) {
        long now = currentTimeMillis();

        List<Programme> programmesByType = contentService.getProgrammesByType(type);

        LOGGER.info("querying by type={} returned {} in {} ms", type, programmesByType.size(), currentTimeMillis() - now);

        return programmesByType;
    }

}
