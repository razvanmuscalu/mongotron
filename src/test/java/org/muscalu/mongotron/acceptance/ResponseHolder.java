package org.muscalu.mongotron.acceptance;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseHolder {

    private ResponseEntity<String> response;

    public void set(ResponseEntity<String> response) {
        this.response = response;
    }

    public String getBody() {
        return response.getBody();
    }

}
