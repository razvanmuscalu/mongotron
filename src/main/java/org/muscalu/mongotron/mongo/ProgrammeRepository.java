package org.muscalu.mongotron.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProgrammeRepository extends MongoRepository<MongoProgramme, String> {

    MongoProgramme findByTitle(String title);
    List<MongoProgramme> findByType(String type);

}
