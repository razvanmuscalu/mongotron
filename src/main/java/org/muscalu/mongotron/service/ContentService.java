package org.muscalu.mongotron.service;

import org.muscalu.mongotron.mongo.*;
import org.muscalu.mongotron.web.Programme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class ContentService {

    private final ProgrammeRepository programmeRepository;

    @Autowired
    public ContentService(ProgrammeRepository programmeRepository) {
        this.programmeRepository = programmeRepository;
    }

    public Programme getProgrammeByTitle(String title) {
        MongoProgramme mongoProgramme = programmeRepository.findByTitle(title);

        return new Programme(mongoProgramme.getTitle(), mongoProgramme.getType());
    }

    public List<Programme> getProgrammesByType(String type) {
        List<MongoProgramme> mongoProgrammes = programmeRepository.findByType(type);

        return mongoProgrammes
                .stream()
                .map(mp -> new Programme(mp.getTitle(), mp.getType()))
                .collect(toList());
    }
}
