package org.muscalu.mongotron.acceptance.steps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import org.muscalu.mongotron.acceptance.Sender;
import org.muscalu.mongotron.acceptance.SpringTest;
import org.muscalu.mongotron.mongo.MongoProgramme;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@SpringTest
public class KafkaStepDefs {

    private static final String KAFKA_TOPIC = "metadata";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private Sender sender;

    @Given("^Kafka Producer sends the following programmes$")
    public void kafkaProducerSendsFollowingProgrammes(DataTable dataTable) throws Exception {
        List<MongoProgramme> mongoProgrammes = dataTable.asList(MongoProgramme.class);

        mongoProgrammes.forEach(p -> {
            try {
                sender.sendMessage(KAFKA_TOPIC, OBJECT_MAPPER.writeValueAsString(p));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

}
