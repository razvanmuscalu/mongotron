package org.muscalu.mongotron.acceptance.steps;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import org.muscalu.mongotron.acceptance.SpringTest;
import org.muscalu.mongotron.mongo.MongoProgramme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

@SpringTest
public class MongoStepDefs {

    @Autowired
    private MongoRepository<MongoProgramme, String> mongoRepository;

    @Before
    public void setUp() {
        mongoRepository.deleteAll();
    }

    @Given("^Mongo contains the following programmes$")
    public void mongoHasFollowingProgrammes(DataTable dataTable) throws Exception {
        List<MongoProgramme> mongoProgrammes = dataTable.asList(MongoProgramme.class);

        mongoProgrammes.forEach(p -> mongoRepository.save(p));
    }
}
