Feature: Mongo

  Scenario: Should return by title
    Given Mongo contains the following programmes
      | title    | type   |
      | Avengers | movie  |
      | Arrival  | movie  |
      | Sopranos | series |

    When I call the Content API for Avengers title

    Then The following programmes should be returned
      | title    | type  |
      | Avengers | movie |


  Scenario: Should return by type
    Given Mongo contains the following programmes
      | title    | type   |
      | Avengers | movie  |
      | Arrival  | movie  |
      | Sopranos | series |

    When I call the Content API with type=movie query

    Then The following programmes should be returned
      | title    | type  |
      | Avengers | movie |
      | Arrival  | movie |