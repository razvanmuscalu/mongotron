package org.muscalu.mongotron.acceptance;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber-html-reports-with-screenshots", "json:target/cucumber-report.json"},
        monochrome = true,
        features = "src/test/resources/features",
        glue = "classpath:org.muscalu.mongotron.acceptance",
        tags = "~@Ignore")
public class RunCukesTest {

}