Feature: Kafka

  Scenario: Should return by title
    Given Kafka Producer sends the following programmes
      | title    | type   |
      | Avengers | movie  |
      | Arrival  | movie  |
      | Sopranos | series |

    When I call the Content API for Avengers title

    Then The following programmes should be returned
      | title    | type  |
      | Avengers | movie |