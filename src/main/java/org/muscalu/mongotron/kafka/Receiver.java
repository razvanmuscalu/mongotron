package org.muscalu.mongotron.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.muscalu.mongotron.mongo.MongoProgramme;
import org.muscalu.mongotron.mongo.ProgrammeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

@Component
public class Receiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final CountDownLatch latch = new CountDownLatch(1);
    private final ProgrammeRepository programmeRepository;

    @Autowired
    public Receiver(ProgrammeRepository programmeRepository) {
        this.programmeRepository = programmeRepository;
    }

    @KafkaListener(topics = "metadata")
    public void receiveMessage(String message) throws IOException {
        LOGGER.info("received message: {}", message);

        MongoProgramme mongoProgramme = OBJECT_MAPPER.readValue(message, MongoProgramme.class);

        LOGGER.info("mapped message: {}", mongoProgramme);

        programmeRepository.save(mongoProgramme);

        latch.countDown();
    }

}
